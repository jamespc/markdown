---
title: About ACM@USC
date: 2019-01-03
author: Brady O'Leary
tag: <span class="tag blue"> Info </span>
summary: We are the Association of Computing Machinery Student Chapter at the University of South Carolina. We meet every Wednesday in the ACM Room (there's usually Pizza), Swearingen 2A17, to discuss topics in technology, computing, and student projects. We will also hold Special Interest Meetings throughout the semester where Special Guests will give talks on various subjects in Computing.
---
<section class="titlecard blue-green">
  <h1> About ACM@USC </h1>
</section>
<nav>
  <div class="container">
    <span> Section: </span>
    <ul>
      <li> <a href="#Who%20We%20Are">Who we Are</a> </li>
      <li> <a href="#When%20we%20meet">When we Meet</a> </li>
      <li> <a href="#Events">Events</a> </li>
      <li> <a href="#Community%20Service">Community Service</a> </li>
      <li> <a href="#Contact%20Us">Contact Us</a> </li>
    </ul>
  <div>
</nav>

  <div class="container">

# Who We Are
We are a Student Organization that regularly meets to share our interest in various subjects in computing. We welcome people of all majors and backgrounds to come out and learn more about various topics in computing. Many of our talks tend to focus more on various types of Software and Programming Languages, but occasionally we have talks on hardware and other topics as well. We are certainly open to students wanting to give talks.

# When we meet
We hold regular meetings throughout the semester on Wednesdays at 7:00pm in the ACM Room (Swearingen 2A17) where there is usually pizza. These meetings usually consist of one of our students giving a talk on a great variety of computing-related subjects. You can find more information about what these meetings are about by checking our [home page](/), as we will post on upcoming meeting topics.

# Events
We hold a few special events throughout each semester.

## Code-a-thon
The Code-a-thon is a 24-hour event where students compete to see who can solve the most coding problems the fastest, with prizes being given to the top 3 students in each division. There are 2 divisions, of which are split into 2 sub-categories depending on which CSCE class was last taken (out of CSCE145, CSCE146, CSCE240, and CSCE350. Students who have never taken any of these classes will fall into the CSCE145 category) in order to keep the content more fair to less experienced students. The competition is held in the Swearingen Windows Computer Labs on the 1st floor of Swearingen in D-Wing, however, since the competition is held online, students are not actually required to attend in person, and may compete from wherever they choose. You will hear more about this as we get close to the event.

## Special Interest Meetings
Occasionally a professor or person involved in the Industry will give a talk on a topic. Typically, these may happen once or twice a semester, and will usually occur on a different day than the normal meetings.

## Hack-a-thon
In the Spring of 2018, the Computer Science Department, with help from our student chapter and a few other Computing Student Organizations, put on ColaHacks, an event where students from a multitude of Colleges and Universities had 12 hours to completely write a piece of software that would help the people of Columbia, SC. Check the [home page](/) for more info during the Spring Semester for more information about this event.

# Community Service
In the Spring, we generally hold a Fix-It-Day, where members of this organization take a Saturday to fix Computers and also recycle old computers for free in order to give back to the Columbia Community.

# Contact Us
If you are interested in learning more about our ACM Chapter, or are interested in sponsoring an event, we can be reached at the following emails:

[soACM@mailbox.sc.edu](mailto:soACM@mailbox.sc.edu)

[uofscacm@gmail.com](mailto:uofscacm@gmail.com)

You should hear back in a few days from sending the email, as both are regularly checked.

  </div>
