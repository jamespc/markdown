---
title: This is a Test
date: 2019-01-03
author: Brady O'Leary
tag: <span class="tag"> Static Site Generation </span>
summary: This is a test of our new web-publishing system. It uses <a href="https://www.romanzolotarev.com/ssg.html"> SSG4 </a> and some Python to generate the static content from Markdown Files. Pretty Riveting Stuff!
---

  <div class="container">

# Static Site Generator Test!

This is a test of our new Static Site Generator, based on [SSG4](https://www.romanzolotarev.com/ssg.html), with some python extensions written by me. If you're interested, check out [the repository](https://github.com/Nesdood007/ssgtools). It's pretty cool.

  </div>
