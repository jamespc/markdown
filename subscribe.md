---
title: Subscribe to the ListServ
date: 2019-02-10
author: Brady O'Leary
tag: <span class="tag green"> #Subscribe </span><span class="tag blue"> #ShamelessPlug </span>
summary: Here are some Instructions to subscribe to the listserv.
---

<section class="titlecard blue-green">
  <div class="container">
    <h1> Subscribing to the ListServ </h1>
    <hr>
    <p> The Easiest way to keep up with events put on by ACM@USC </p>
  </div>
</section>

  <div class="container">

The Best way to keep up with ACM@USC events is to subscribe to our ListServ. This allows you to recieve regular updates about upcoming events and opportunities without having to constantly check the website. We will not send out spam.

# How to Subscribe

The easiest way to subscribe to our list is to simply come to the next meeting. We automatically add new people to the mailing list with the given email ont he sign-in sheet.

<a class="button garnet" href="http://listserv.sc.edu/cgi-bin/wa?SUBED1=UOFSCACM&A=1"> Subscribe </a>

The Above Link should take you to a form on the Listserv to sign up for our mailing list. It is easiest to simply fill out your Name, Email, and then hit that subscribe button. Alternatively, you can Login, however that usually takes longer than just filling out the form.

# What's in the emails?

These emails contain updates and informations on the happenings of the club, focusing on nearby events. We generally send out emails once or twice a week to remind our members of events happening with the club, and to also share opportunities available to our members as well. You really have nothing to lose from subscribing to our mailing list, and recieveing updates.

# How do I stop recieving emails?

We include an unsubscribe link in the bottom of our emails. You can also go to [listserv.sc.edu](http://listserv.sc.edu), login, and unsubscribe from our mailing list.

  </div>
