---
title: Fall 2019 Code-a-thon
date: 2019-10-18
author: Joshua Nelson
tag: <span class="tag"> code-a-thon </span> <span class="tag red"> #ThereWillBePizzaToo! </span> 🍕🍕🍕🍕🍕
summary: Join us for the 2019 Fall Code-a-thon!
---

<!-- You can and should use HTML in the summary if needed. This will be injected into the template -->

<section class="titlecard blue-green">
  <div class="container">
    <h1> Fall 2019 Code-a-thon </h1>
  </div>
</section>

  <div class="container">

<p class="date"> Friday, October 18th, 2019 </p>
<p class="time"> 7:00pm </p>
<p class="location"> Swearingen 2D11 </p>

The Fall 2019 Code-a-thon is here!

Join the Association for Computing Machinery (ACM) for a programming competition.
Solve problems, eat pizza, and find internships at our most anticipated event of the semester!

We have 4 different divisions so that you do not have to compete with people of wildly different skill levels.
Once the competition starts, you can click on the links to join the division:

- [145](http://www.hackerrank.com/acmusc-codeathon-145-f19), introductory programming questions
- [146](http://www.hackerrank.com/acmusc-codeathon-146-f19), data structures questions
- [240](http://www.hackerrank.com/acmusc-codeathon-240-f19), algorithms questions
- [350](https://www.hackerrank.com/acmusc-codeathon-350-f19), advanced algorithms questions

The code-a-thon runs 24 hours (7 PM - 7 PM),
but you do not have to be present for all of it.
Submitting problems from home is allowed and welcomed,
especially for alumni not in Columbia.
There will be pizza, snacks, and drinks at the event.

If you come in 1st, 2nd, or 3rd place for a division, you will win a prizes.
Prizes have not yet been determined, but in the past have been Arduinos, gift cards,
flash drives, and USB keyboards.

Working in groups is allowed, but groups cannot win prizes.

Note that there is usually a great deal of overlap between questions for divisions.
As such, you can only win prizes for the first division in which you submitted a problem.

Here are the problems from the Fall 2018 Code-a-thon:

- [240 and 350](https://github.com/JamesPC44/USC_UpperDivision_Fall2018/)
- [145 and 146](https://github.com/JamesPC44/USC_LowerDivision_Fall2018/)

We hope to see you at the Code-a-thon!

  </div>
