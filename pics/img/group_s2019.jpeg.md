---
title: Spring 2019 ACM Group Picture
imgpath: img/group_s2019.jpeg
grid: 1x2
date: 2019-04-24
---
<div class="container">
    <h1> group_s2019.jpeg </h1>
    <figure>
        <a href="group_s2019.jpeg"><img src="group_s2019.jpeg"></a>
        <figcaption>
            <span class="figure"> group_s2019.jpeg: </span> <mark>Your Description Should Go Here!</mark>
        </figcaption>
    </figure>
</div>
